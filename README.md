**TrashbusterのUnityプロジェクトについて**

Unity 2021.2.11

---

## SauceTreeの適用

bitbucketをSauceTreeで使用するための手順

1. このページの右上の**「クローンの作成」**をクリック
2. **「Clone in SauceTree」**をクリック
3. SauceTreeが起動したら「Credential Helper Selector」の**「manager」**を選択しログインする
4. 保存先のパスは**自身のPCのドキュメント等**に新しくフォルダを作成しましょう　例:「TrashBusterClone」
5. 詳細オプションを開いて**「develop」**を選択してクローン

---

## Unityプロジェクトの設定

作ったクローンリポジトリをUnityのプロジェクトとして開く手順 ※指定バージョンがない場合[ダウンロードアーカイブ](https://unity3d.com/jp/get-unity/download/archive)から選択してダウンロードしてください

1. UnityHubを開き**「リストに追加」**をクリック
2. クローンリポジトリのフォルダを選択
3. バージョンが指定できていなければ**「Unity 2020.1.0f1」**に設定
4. 新しく出てきたunityProjectを開く